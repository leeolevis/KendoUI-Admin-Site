$(function () {
    // 普通拖放
    $('#generalDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
            $('#generalDropTarget').text('拖过来~').removeClass('theme-s-bg');
        },
        dragend: function (e) {
            if (!e.sender.dropped) {
                $('#generalDropTarget').text('再来一次呗~');
            }
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    $('#generalDropTarget').kendoDropTarget({
        dragenter: function (e) {
            $(e.dropTarget).text('放下来~').addClass('theme-s-bg');
        },
        dragleave: function (e) {
            $(e.dropTarget).text("拖过来~").removeClass('theme-s-bg');
        },
        drop: function (e) {
            $(e.dropTarget).text('你真棒！');
            $(e.draggable).removeClass('theme-m-box');
        }
    });
    // 指定区域拖放
    $('#areaDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
            $('#areaLeft, #areaRight').text('拖过来~').removeClass('theme-s-bg');
        },
        dragend: function (e) {
            if (!e.sender.dropped) {
                $('#areaLeft, #areaRight').text('再来一次呗~');
            }
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    $('#areaDropTarget').kendoDropTargetArea({
        filter: '#areaLeft, #areaRight',
        dragenter: function (e) {
            $(e.dropTarget).text('放下来~').addClass('theme-s-bg');
        },
        dragleave: function (e) {
            $(e.dropTarget).text("拖过来~").removeClass('theme-s-bg');
        },
        drop: function (e) {
            $(e.dropTarget).text('你真棒！');
            $(e.draggable).removeClass('theme-m-box');
        }
    });
    // 限定范围拖放
    $('#containerDragDrop').kendoDraggable({
        container: $('#rangeDragDrop'),
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    // 水平拖放
    $('#horizontalDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        axis: 'x',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    // 垂直拖放
    $('#verticalDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        axis: 'y',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    // 偏移拖放
    $('#offsetDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        cursorOffset: {
            top: 20,
            left: 20
        },
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    // 自动滚动拖放
    $('#scrollDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        autoScroll: true,
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    // 指定元素拖放
    $('#filterDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        filter: '.canDragDrop',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    // 忽略元素拖放
    $('#ignoreDragDrop').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        ignore: '#insideDragDrop',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    // 分组拖放
    $('.groupDragDropLeft').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        group: 'leftGroup',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
            $('#groupDropTargetLeft').text('拖过来~');
        },
        dragend: function (e) {
            if (!e.sender.dropped) {
                $('#groupDropTargetLeft').text('再来一次呗~');
            }
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    $('.groupDragDropRight').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        group: 'rightGroup',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-s-box');
            $('#groupDropTargetRight').text('拖过来~');
        },
        dragend: function (e) {
            if (!e.sender.dropped) {
                $('#groupDropTargetRight').text('再来一次呗~');
            }
            $(e.currentTarget).removeClass('theme-s-box');
        }
    });
    $('#groupDropTargetLeft').kendoDropTarget({
        group: 'leftGroup',
        dragenter: function (e) {
            $(e.dropTarget).text('放下来~');
        },
        dragleave: function (e) {
            $(e.dropTarget).text("拖过来~");
        },
        drop: function (e) {
            $(e.dropTarget).text('你真棒！');
            $(e.draggable).removeClass('theme-m-box');
        }
    });
    $('#groupDropTargetRight').kendoDropTarget({
        group: 'rightGroup',
        dragenter: function (e) {
            $(e.dropTarget).text('放下来~');
        },
        dragleave: function (e) {
            $(e.dropTarget).text("拖过来~");
        },
        drop: function (e) {
            $(e.dropTarget).text('你真棒！');
            $(e.draggable).removeClass('theme-s-box');
        }
    });
    // 区域分组拖放
    $('.areaGroupDragDropLeft').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        group: 'leftGroup',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-m-box');
            $('#areaGroupDropTargetLeftTop, #areaGroupDropTargetLeftBottom').text('拖过来~');
        },
        dragend: function (e) {
            if (!e.sender.dropped) {
                $('#areaGroupDropTargetLeftTop, #areaGroupDropTargetLeftBottom').text('再来一次呗~');
            }
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
    $('.areaGroupDragDropRight').kendoDraggable({
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        group: 'rightGroup',
        dragstart: function (e) {
            $(e.currentTarget).addClass('theme-s-box');
            $('#areaGroupDropTargetRightTop, #areaGroupDropTargetRightBottom').text('拖过来~');
        },
        dragend: function (e) {
            if (!e.sender.dropped) {
                $('#areaGroupDropTargetRightTop, #areaGroupDropTargetRightBottom').text('再来一次呗~');
            }
            $(e.currentTarget).removeClass('theme-s-box');
        }
    });
    $('#areaGroupDropTargetLeft').kendoDropTargetArea({
        group: 'leftGroup',
        filter: '#areaGroupDropTargetLeftTop, #areaGroupDropTargetLeftBottom',
        dragenter: function (e) {
            $(e.dropTarget).text('放下来~');
        },
        dragleave: function (e) {
            $(e.dropTarget).text("拖过来~");
        },
        drop: function (e) {
            $(e.dropTarget).text('你真棒！');
            $(e.draggable).removeClass('theme-m-box');
        }
    });
    $('#areaGroupDropTargetRight').kendoDropTargetArea({
        group: 'rightGroup',
        filter: '#areaGroupDropTargetRightTop, #areaGroupDropTargetRightBottom',
        dragenter: function (e) {
            $(e.dropTarget).text('放下来~');
        },
        dragleave: function (e) {
            $(e.dropTarget).text("拖过来~");
        },
        drop: function (e) {
            $(e.dropTarget).text('你真棒！');
            $(e.draggable).removeClass('theme-s-box');
        }
    });
    // 移动端拖放
    $('#mobileDragDrop').kendoDraggable({
        holdToDrag: true,
        hold: function (e) {
            $(e.currentTarget).addClass('theme-s-bg');
        },
        hint: function (element) {
            return element.clone().css('opacity', .6);
        },
        dragstart: function (e) {
            $(e.currentTarget).removeClass('theme-s-bg').addClass('theme-m-box');
        },
        dragend: function (e) {
            $(e.currentTarget).removeClass('theme-m-box');
        }
    });
});